#!/bin/sh

SCRIPT=$(readlink -f "$0")
cd ${SCRIPT%/*}/..

case $1 in
	ping) docker-compose exec mqtt mosquitto_pub -h localhost \
		-t git/aports/master -m ping ;;
	sub) docker-compose exec mqtt mosquitto_sub -h localhost -t "#" ;;
	"") echo "Provide a subcommand ping|sub";;
	*) echo "\"$1\" unknown command"
esac	
